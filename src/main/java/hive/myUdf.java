package hive;

import eu.bitwalker.useragentutils.*;
import org.apache.hadoop.hive.ql.exec.UDF;
import org.apache.hadoop.hive.serde2.objectinspector.primitive.JavaStringObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.primitive.PrimitiveObjectInspectorFactory;

import java.util.ArrayList;

/**
 * Created by Semashkin on 12.08.2016.
 */
public final class myUdf extends UDF {
    private JavaStringObjectInspector stringInspector;
    public ArrayList<String> evaluate(String userAgentString){
        stringInspector = PrimitiveObjectInspectorFactory.javaStringObjectInspector;
        ArrayList<String> values = new  ArrayList<String>();
        try {
            UserAgent userAgent = UserAgent.parseUserAgentString(userAgentString);
            values.add(0,stringInspector.getPrimitiveJavaObject(userAgent.getBrowser().getName()));
            BrowserType browserType = userAgent.getBrowser().getBrowserType();
            values.add(1,stringInspector.getPrimitiveJavaObject(browserType != null ? browserType.getName() : "Unknown"));
            Version version = userAgent.getBrowserVersion();
            values.add(2,stringInspector.getPrimitiveJavaObject(version != null ? version.getVersion() : "Unknown"));
            OperatingSystem operatingSystem = userAgent.getOperatingSystem();
            values.add(3,stringInspector.getPrimitiveJavaObject(operatingSystem != null ? operatingSystem.getName() : "Unknown"));
            if (operatingSystem != null){
                DeviceType deviceType = operatingSystem.getDeviceType();
                values.add(4,stringInspector.getPrimitiveJavaObject(deviceType != null ? deviceType.getName() : "Unknown"));
            }
        }
        catch (Exception exc){
            Integer len = values.size();
            values.add(len,exc.toString());
        }
        return values;
    }
}
